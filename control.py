import time, socket, sys
from datetime import datetime as dt
import paho.mqtt.client as paho
import signal
import mraa

leds = []
for i in range(2,10):
    led = mraa.Gpio(i)
    led.dir(mraa.DIR_OUT)
    led.write(1)
    leds.append(led)

class Control(object):
    """docstring for Control"""
    def __init__(self):
        self.MY_NAME = 'Control'
        self.mqtt_client = paho.Client()
        self.mqtt_client.on_connect = self.on_connect
        self.mqtt_client.on_disconnect = self.on_disconnect
        self.mqtt_client.on_log = self.on_log
        self.mqtt_topic = 'kappa/control'
        self.mqtt_client.will_set(self.mqtt_topic, '______________Will of '+self.MY_NAME+' _________________\n\n', 0, False)
        self.mqtt_client.connect('sansa.cs.uoregon.edu', '1883',keepalive=300)
        self.mqtt_client.subscribe([('kappa/turnstile', 0), ('kappa/car', 0)])
        self.mqtt_client.message_callback_add('kappa/turnstile', self.on_message_from_turnstile)
        self.mqtt_client.message_callback_add('kappa/car', self.on_message_from_car)
        self.mqtt_client.loop_start()

        self.isTurnstileConnected = False
        self.isDisconnected = False

        self.passengerCount = 0 # Current passenger count on Turnstile
        self.MAX_PASSENGERS = 3 # Maximum Passengers allowed
        #self.NUMBER_OF_CARS = 5
        #self.carsStatuses = [False] * self.NUMBER_OF_CARS # indicates status of the car - is car busy
        self.carsStatuses = {}

        self.sendReadyMsg()

        signal.signal(signal.SIGINT, self.control_c_handler)

    # Deal with control-c
    def control_c_handler(self, signum, frame):
        self.isDisconnected = True
        self.mqtt_client.disconnect()
        self.mqtt_client.loop_stop()  # waits until DISCONNECT message is sent out
        print ("Exit")
        sys.exit(0)
        
    def on_connect(self, client, userdata, flags, rc):
        pass

    def sendReadyMsg(self):
        while not self.isTurnstileConnected and not self.isDisconnected:
            self.mqtt_client.publish(self.mqtt_topic, "isReady")
            time.sleep(1)
        

    def on_message_from_turnstile(self, client, userdata, msg):
        if msg.payload == 'ack':
            self.isTurnstileConnected = True
            self.sendLoadPassengerMsg()
        elif msg.payload == 'passengerLoaded':
            self.turnOnLED()
            self.passengerCount += 1
            self.sendLoadPassengerMsg()

    def turnOnLED(self):
        leds[self.passengerCount].write(0)

    def turnOffAllLEDs(self):
        map(lambda led: led.write(1), leds)

    def sendLoadPassengerMsg(self):
        print('sending LoadPassengerMsg')
        if self.passengerCount < self.MAX_PASSENGERS:
            self.mqtt_client.publish(self.mqtt_topic, 'loadPassenger')
        else:
            print('MAX_PASSENGERS reached')
            self.sendPickupMsg()

    def sendPickupMsg(self):
        print (self.carsStatuses)
        for car_idx, isBusy in self.carsStatuses.items():

            if not isBusy:
                self.carsStatuses[car_idx] = True
                print('sending PickupMsg to car '+str(car_idx))
                self.mqtt_client.publish(self.mqtt_topic, str(car_idx)+'.pick_up')
                self.passengerCount = 0
                self.turnOffAllLEDs()
                self.sendLoadPassengerMsg()
                break
            

    def on_message_from_car(self, client, userdata, msg):
        if len(msg.payload.split('.')) > 1:
            msg_car_id, msg_content = msg.payload.split('.')
            if msg_content == 'readyForNextPassengers' :
                self.carsStatuses[msg_car_id] = False
                if self.passengerCount == self.MAX_PASSENGERS:
                    self.sendPickupMsg()
            if msg_content == 'Ready' :
                print(msg_car_id+'.Ack')
                self.mqtt_client.publish(self.mqtt_topic, msg_car_id+'.Ack')
                self.carsStatuses[msg_car_id] = False

    def on_disconnect(self, client, userdata, rc):
        pass

    def on_log(self, client, userdata, level, buf):
        pass
       #print("log: {}".format(buf)) # only semi-useful IMHO



def main():
    control = Control()
    while True:
        time.sleep(10)

main()