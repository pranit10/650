import time, socket, sys
from datetime import datetime as dt
import paho.mqtt.client as paho
import signal
import mraa

leds = []
for i in range(2,10):
    led = mraa.Gpio(i)
    led.dir(mraa.DIR_OUT)
    led.write(1)
    leds.append(led)

class Car(object):
    """docstring for Car"""
    def __init__(self, car_id, led_no):
        self.MY_NAME = 'Car'
        self.car_id = car_id
        self.led_no = led_no
        self.mqtt_client = paho.Client()
        self.mqtt_client.on_connect = self.on_connect
        self.mqtt_client.on_message = self.on_message
        self.mqtt_client.on_disconnect = self.on_disconnect
        self.mqtt_client.on_log = self.on_log
        self.mqtt_topic = 'kappa/' + 'car'
        self.mqtt_client.will_set(self.mqtt_topic, '______________Will of '+self.MY_NAME+' _________________\n\n', 0, False)
        self.mqtt_client.connect('sansa.cs.uoregon.edu', '1883',keepalive=300)
        self.mqtt_client.subscribe('kappa/control')
        self.mqtt_client.loop_start()
        self.controlAck = False
        self.isDisconnected = False

        signal.signal(signal.SIGINT, self.control_c_handler)
        self.sendCarDetails()

    def sendCarDetails(self):
        while not self.controlAck and not self.isDisconnected:
            print(self.car_id+'.Ready')
            self.mqtt_client.publish(self.mqtt_topic, self.car_id+'.Ready')
            time.sleep(3)

    # Deal with control-c
    def control_c_handler(self, signum, frame):
        self.isDisconnected = True
        self.mqtt_client.disconnect()
        self.mqtt_client.loop_stop()  # waits until DISCONNECT message is sent out
        print ("Exit")
        sys.exit(0)
        
    def on_connect(self, client, userdata, flags, rc):
        pass
    
    def on_message(self, client, userdata, msg):
        if len(msg.payload.split('.')) > 1:
            msg_car_id, msg_content = msg.payload.split('.')
            if msg_car_id == self.car_id :
                if msg_content == 'pick_up' :
                    self.goForRide()
                if msg_content == 'Ack':
                    self.controlAck = True  

    def goForRide(self):
        f = open('car_input.txt', 'r')
        lines = f.readlines()
        f.close()
        self.turnOnLED()
        for user_input in lines:
            user_input = user_input.strip()
            if user_input == '1':
                self.blinkLED()
                print("Car " + self.car_id + ": Let's go for ride!!")
            elif user_input == '2':
                print("Car " + self.car_id + ": That was fun! We have arrived")
                print("Car " + self.car_id + ": Glad we have a new social group")
                time.sleep(6)
                break
            else:
                print("Car " + self.car_id + ": Please enter correct input!!")

        self.sendReadyForNextPassengers()

    def turnOnLED(self):
        led_idx = int(self.led_no)
        leds[led_idx].write(0)

    def turnOffLED(self):
        led_idx = int(self.led_no)
        leds[led_idx].write(1)

    def blinkLED(self):
        for x in xrange(0,30):
            self.turnOnLED()
            time.sleep(0.1)
            self.turnOffLED()
            time.sleep(0.1)

    def sendReadyForNextPassengers(self):
        #print('sending readyForNextPassengers')
        self.turnOffLED()
        self.mqtt_client.publish(self.mqtt_topic, self.car_id+'.readyForNextPassengers')

    def on_disconnect(self, client, userdata, rc):
        pass

    def on_log(self, client, userdata, level, buf):
        pass
        #print("log: {}".format(buf)) # only semi-useful IMHO


def main():
    arr = sys.argv
    if  len (arr) != 3 :
        print ('Please enter valid input, e.g. python car.py <car_id> <led_no>')
        sys.exit(1)
    if arr[2] not in '12345678' or len(arr[2]) > 1:
        print ('Please enter valid led number between 1 to 8')
        sys.exit(1)
    Car(arr[1], arr[2])
    while True:
        time.sleep(10)

main()