import time, socket, sys
from datetime import datetime as dt
import paho.mqtt.client as paho
import signal


class Turnstile(object):
    """docstring for Turnstile"""
    def __init__(self):
        self.MY_NAME = 'Turnstile'
        self.mqtt_client = paho.Client()
        self.mqtt_client.on_connect = self.on_connect
        self.mqtt_client.on_message = self.on_message
        self.mqtt_client.on_disconnect = self.on_disconnect
        self.mqtt_client.on_log = self.on_log
        self.mqtt_topic = 'kappa/turnstile'
        self.mqtt_client.will_set(self.mqtt_topic, '______________Will of '+self.MY_NAME+' _________________\n\n', 0, False)
        self.mqtt_client.connect('sansa.cs.uoregon.edu', '1883',keepalive=300)
        self.mqtt_client.subscribe('kappa/control')
        self.mqtt_client.loop_start()

        self.isDisconnected = False

        signal.signal(signal.SIGINT, self.control_c_handler)

    # Deal with control-c
    def control_c_handler(self, signum, frame):
        self.isDisconnected = True
        self.mqtt_client.disconnect()
        self.mqtt_client.loop_stop()  # waits until DISCONNECT message is sent out
        print ("Exit")
        sys.exit(0)
        
    def on_connect(self, client, userdata, flags, rc):
        pass
    
    def on_message(self, client, userdata, msg):
        if msg.payload == "isReady":
            self.mqtt_client.publish(self.mqtt_topic, "ack")
        if msg.payload == 'loadPassenger':
            while True and not self.isDisconnected:
                user_input = raw_input('Press Y to Enter Wonderful ride on Roller Coaster,\nN if you are planning for next time\n')
                if user_input.lower() == 'y':
                    print('Welcome!\n\n')
                    self.mqtt_client.publish(self.mqtt_topic, 'passengerLoaded')
                    break
                elif user_input.lower() == 'n':
                    print('Alright! See you next time!\n\n')
                else:
                    print('Please enter correct input\n\n')

    def on_disconnect(self, client, userdata, rc):
        pass

    def on_log(self, client, userdata, level, buf):
        pass
        #print("log: {}".format(buf)) # only semi-useful IMHO

def main():
    turnstile = Turnstile()
    while True:
        time.sleep(10)

main()